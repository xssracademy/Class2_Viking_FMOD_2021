﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideVCA : MonoBehaviour
{
    UnityEngine.UI.Slider ourSlider;
    FMOD.Studio.VCA vcaSlider;
    float vcaVolume;
    public string VCAName;
    
    
    // Start is called before the first frame update
    void Start()
    {
        ourSlider = gameObject.GetComponent<UnityEngine.UI.Slider>();
        vcaSlider = FMODUnity.RuntimeManager.GetVCA("vca:/" + VCAName);
        vcaSlider.getVolume(out vcaVolume);
        ourSlider.value = vcaVolume;

    }

    public void VCAVolumeChange()
    {
        vcaSlider.setVolume(ourSlider.value);
    
    
    }
}
