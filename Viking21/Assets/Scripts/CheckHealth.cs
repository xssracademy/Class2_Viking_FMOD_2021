﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController; // поле имет контроллера

public class CheckHealth : MonoBehaviour
{
    public vThirdPersonController controller; 

    [FMODUnity.EventRef]
    public string snapshotevent;

    FMOD.Studio.EventInstance snapshotinstance;
    public FMOD.Studio.PLAYBACK_STATE state;

    void Start()
    {
        controller = gameObject.GetComponent<vThirdPersonController>();
        snapshotinstance = FMODUnity.RuntimeManager.CreateInstance(snapshotevent);
    
    }

    public void HealthSnapshotStart()
    {
        snapshotinstance.getPlaybackState(out state);
        if (state != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            snapshotinstance.start();
        }
    }
 
    public void HealthSnapshotStop()
    {
        if (controller.currentHealth >= 30)
        {
            snapshotinstance.getPlaybackState(out state);
            if (state == FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                snapshotinstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
        }
        
    }
}
