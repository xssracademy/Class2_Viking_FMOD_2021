﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Breathing : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string breathEvent;


    FMOD.Studio.EventInstance breathInstance;


    vThirdPersonController tpController;

    // Start is called before the first frame update
    void Start()
    {
        breathInstance = FMODUnity.RuntimeManager.CreateInstance(breathEvent);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(breathInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
        breathInstance.start();
        tpController = GetComponent<vThirdPersonController>();

    }

    // Update is called once per frame
    void Update()
    {

        if (tpController.isJumping)
        {


            Debug.Log("Jumping");
        }

        else
        {
            if (tpController.isSprinting)
            {
                breathInstance.setParameterByName("locomotion_type", 2f);
            }
            else
                breathInstance.setParameterByName("locomotion_type", 1f);
        }
    }

    void jump()
    {
        breathInstance.setParameterByName("locomotion_type", 3f);
        Debug.Log("Jump123");
    }
    void jump_move()
    {
        breathInstance.setParameterByName("locomotion_type", 4f);
        Debug.Log("Jump Move");
    }
   
    void land_low()
    {
        breathInstance.setParameterByName("locomotion_type", 5f);
        Debug.Log("Landing Low");
    }

    void land_high()
    {
        breathInstance.setParameterByName("locomotion_type", 6f);
        Debug.Log("Landing High");
    }

}
